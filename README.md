#Running the app.#
1.	Build the application 
2.	Either 
a.	run the MerchantsGuideToTheGalaxy.ConsoleApp as the start-up application within visual studio or 
b.	run the .exe from \MerchantGuideToTheGalaxy\Source\MerchantsGuideToTheGalaxy.ConsoleApp\bin\Debug
c.	Set the web application as the start-up project and run it from within visual studio
3.	For the  console application. 
a.	To process a file type of copy the file path into the console and press enter
b.	The output will be display in cyan if there is any output
4.	For the web application 
a.	use the file upload and select a file and then click upload or
b.	paste in the content

#Design#
The application is designed in the layers of decreasing probability of change to the application as it is used.
  
## Core ##
The core project contains common interfaces and the symbol classes. 

The symbol classes model the Roman numeral system semantics. The SubtractableSymbol class models symbols that can be added and subtracted.

IDecimalConverter takes in some text and converts that to a decimal equivalent. 

IExpression represents the expression used on the language such as glob is I

IExpressionInterpter uses IExpression to process expressions

## Symbols ##
The symbols project contains classes that represent the Roman numeral system syntaxes and a converter that uses these syntaxes and the semantics in core to convert to decimal.

The advantage of the symbol classes is that we could create a syntax based on the galactic language and apply the same semantics without having to first translate the expression.

The converter is based on http://www.dofactory.com/net/interpreter-design-pattern

## Expressions ##
The expressions project contains classes that represent expression and an interpreter that takes an expression tree(an ordered collection of expressions) and interprets the text provided. 
The interpreter is based on http://www.dofactory.com/net/interpreter-design-pattern
## App.Core ##
The app.core project contains interfaces that are not part of the domain but rather for the technology. It tries to follow an MVC style pattern. This would potentially allow different technologies to be used web or desktop.

It also contains IFileWrapper for making testing easier.
## App ##
The app project contains implementations of the App.Core project. It contains a ConsoleView and implementation of the IFileWrapper. I also contains a rudimentary IoC resolver.
## AppConsole ##
AppConsole is a console app.
## WebApp ##
Web application need more acceptance tests using selenium webdriver.