using MerchantsGuideToTheGalaxy.Core;

namespace MerchantsGuideToTheGalaxy.Symbols
{
    public class FiveHundredSymbol :Symbol
    {
        public FiveHundredSymbol():base("D", 500) { }
        public FiveHundredSymbol(string identifier):base(identifier, 500) { }
    }
}