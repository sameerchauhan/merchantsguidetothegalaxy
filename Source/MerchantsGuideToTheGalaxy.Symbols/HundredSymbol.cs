using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantsGuideToTheGalaxy.Symbols
{
    public class HundredSymbol : SubtractableSymbol
    {
        public HundredSymbol() : base("C", 100, new List<Symbol> {new FiveHundredSymbol(), new ThousandSymbol()})  { }

        public HundredSymbol(string identifier, List<Symbol> subtractableFromSymbol) : base(identifier, 100, subtractableFromSymbol) { }
    }
}