using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantsGuideToTheGalaxy.Symbols
{

    public class OneSymbol : SubtractableSymbol
    {
        public OneSymbol() : base("I", 1, new List<Symbol>{ new FiveSymbol(), new TenSymbol()}) { }

        public OneSymbol(string identifier, List<Symbol> subtractable) : base(identifier, 1, subtractable) { }
    }
}