using MerchantsGuideToTheGalaxy.Core;

namespace MerchantsGuideToTheGalaxy.Symbols
{
    public class FiveSymbol :Symbol
    {
        public FiveSymbol():base("V", 5) { }

        public FiveSymbol(string identifier) : base(identifier, 5) { }
    }
}