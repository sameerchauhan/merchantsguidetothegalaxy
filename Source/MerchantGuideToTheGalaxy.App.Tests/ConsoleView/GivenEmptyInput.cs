using System;
using System.Collections.Generic;
using System.IO;
using MerchantGuideToTheGalaxy.Test.Helpers;
using MerchantsGuideToTheGalaxy.Core;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.App.Tests.ConsoleView
{
    [TestFixture]
    public class GivenEmptyInput
    {
        private StringWriter _streamOut;
        private StringReader _stream;

        [SetUp]
        public void SetUp()
        {
            _streamOut = new StringWriter();
            _stream = new StringReader(Environment.NewLine);

            Console.SetIn(_stream);
            Console.SetOut(_streamOut);
            var view = new MerchantGuideToTheGalaxy.App.ConsoleView(new MerchantGuideToTheGalaxy.App.FileWrapper());
            
            view.Initialise(null);
        }

        [Test]
        public void ShouldAskUserToTryAgain()
        {
            var actual = TestHelper.CheckStream(3, _streamOut);
            Assert.That(actual.Trim(), Is.EqualTo("No information has been entered, please try again."));
        }


        [TearDown]
        public void Cleanup()
        {
            _stream.Close();
            _stream = null;
            _streamOut.Close();
            _streamOut = null;
        }
    }
}