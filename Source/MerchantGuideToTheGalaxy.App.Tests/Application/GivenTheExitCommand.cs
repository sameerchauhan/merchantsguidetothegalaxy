using System;
using System.Collections.Generic;
using System.IO;
using MerchantGuideToTheGalaxy.Expressions;
using MerchantGuideToTheGalaxy.Test.Helpers;
using MerchantsGuideToTheGalaxy.Core;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.App.Tests.Application
{
    [TestFixture]
    public class GivenTheExitCommand
    {
        private StringReader _stream;
        private StringWriter _streamOut;

        [SetUp]
        public void Setup()
        {
            _stream = new StringReader("exit");
            _streamOut = new StringWriter();

            Console.SetOut(_streamOut);
            Console.SetIn(_stream);
            var fileWrapper = new MerchantGuideToTheGalaxy.App.FileWrapper();
            var view = new MerchantGuideToTheGalaxy.App.ConsoleView(fileWrapper);
            var priceList = new Dictionary<string, decimal>();
            var dictionary = new Dictionary<string, string>();
            var output = new List<string>();
            var decimalConverter = DecimalConverter.CreateDefault();
            var translator = new Translator(dictionary);
            var expressionTree = new List<IExpression>
            {
                new TranslationExpression(dictionary),
                new TransactionExpression(priceList, decimalConverter, translator),
                new QueryExpression( decimalConverter, output, translator),
                new CreditQueryExpression(decimalConverter, priceList, output, translator),
                new UnknownExpression(output)
            };
            var controller = new MerchantGuideToTheGalaxy.App.Controller(view, new ExpressionIntepreter(expressionTree), output);
            var app = new App(controller);
            app.Initialise();
        }

        [TearDown]
        public void Cleanup()
        {
            _stream.Close();
            _stream = null;
            _streamOut.Close();
            _streamOut = null;
        }

        [Test]
        public void ShouldDisplayMessageToEndProgram()
        {
            var actual = TestHelper.CheckStream(2, _streamOut);

            Assert.That(actual, Is.EqualTo("Shutting down application. Please press enter to close the console."));
        }
    }
}