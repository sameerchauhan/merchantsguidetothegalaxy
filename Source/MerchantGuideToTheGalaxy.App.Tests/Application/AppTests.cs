﻿using System;
using System.Collections.Generic;
using System.IO;
using MerchantGuideToTheGalaxy.Expressions;
using MerchantsGuideToTheGalaxy.Core;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.App.Tests.Application
{
    [TestFixture]
    public class AppTests
    {
        private StringReader _stream;
        private StringWriter _streamOut;
        private const string SplitCharacter = ">";
        private const int PositionOfWelcomeMessage = 0;
        private const int PositionOfAskingForInputMessage = 1;
        private const string Path = @"C:\MerchantGuideTests\testfile.txt";

        [OneTimeSetUp]
        public void OneTime()
        {
            if (!Directory.Exists(@"C:\MerchantGuideTests"))
            {
                Directory.CreateDirectory(@"C:\MerchantGuideTests");
            }

            File.Delete(Path);
            if (!File.Exists(Path))
            {
                using (var stream = new FileStream(Path, FileMode.Create, FileAccess.ReadWrite))
                using (var stringWriter = new StreamWriter(stream))
                {
                    stringWriter.WriteLine(@"
glob is I
prok is V
pish is X
tegj is L
glob glob Silver is 34 Credits
glob prok Gold is 57800 Credits
pish pish Iron is 3910 Credits
how much is pish tegj glob glob ?
how many Credits is glob prok Silver ?
how many Credits is glob prok Gold ?
how many Credits is glob prok Iron ?
how much wood could a woodchuck chuck if a woodchuck could chuck wood ?");
                }
            }
        }

        [SetUp]
        public void Setup()
        {
            _stream = new StringReader(Path);
            _streamOut = new StringWriter();

            Console.SetOut(_streamOut);
            Console.SetIn(_stream);
            var fileWrapper = new MerchantGuideToTheGalaxy.App.FileWrapper();
            var view = new MerchantGuideToTheGalaxy.App.ConsoleView(fileWrapper);
            var priceList = new Dictionary<string, decimal>();
            var dictionary = new Dictionary<string, string>();
            var output = new List<string>();
            var decimalConverter = DecimalConverter.CreateDefault();
            var translator = new Translator(dictionary);
            var expressionTree = new List<IExpression>
            {
                new TranslationExpression(dictionary),
                new TransactionExpression(priceList, decimalConverter, translator),
                new QueryExpression( decimalConverter, output, translator),
                new CreditQueryExpression(decimalConverter, priceList, output, translator),
                new UnknownExpression(output)
            };
             var controller = new MerchantGuideToTheGalaxy.App.Controller(view, new ExpressionIntepreter(expressionTree), output);
            var app = new App(controller);
            app.Initialise();
        }

        [TearDown]
        public void Cleanup()
        {
              _stream.Close();
            _stream = null;
              _streamOut.Close();
            _streamOut = null;

        }

        [Test]
        public void ShouldShowWelcomeMessage()
        {
            CheckStream(PositionOfWelcomeMessage, "Welcome to Merchant Guide To Galaxy.");
        }

        [Test]
        public void ShouldAskForInput()
        {
            CheckStream(PositionOfAskingForInputMessage, "Please enter a file path or text and press enter. Type exit to leave.");
        }
     
        [Test]
        public void ShouldReadInputFromUser()
        {
            var actual = _streamOut.ToString()
                .Trim()
                .Split(SplitCharacter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[2]
                .Trim();
            var expected = "Processing {Path}.".Replace("{Path}", Path);

           Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void ShouldProcessFileContents()
        {
            CheckStream(3, @"pish tegj glob glob is 42
glob prok Silver is 68 Credits
glob prok Gold is 57800 Credits
glob prok Iron is 782 Credits
I have no idea what you are talking about");
        }

        private void CheckStream(int position, string expectedMessage)
        {
            var actual = _streamOut.ToString()
                .Trim()
                .Split(SplitCharacter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[position]
                .Trim();

            Assert.That(actual, Is.EqualTo(expectedMessage));
        }
    }
}