﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace MerchantGuideToTheGalaxy.App.Tests.FileWrapper
{
    [TestFixture()]
    public class GivenAValidPathAndFileExists
    {
        private MerchantGuideToTheGalaxy.App.FileWrapper _fileWrapper;
        private string _path;

        [SetUp]
        public void SetUp()
        {
            _fileWrapper = new MerchantGuideToTheGalaxy.App.FileWrapper();
            _path = TestContext.CurrentContext.TestDirectory + @"\FileWrapper\Test.txt";
        }
       
        [Test]
        public void ShouldReturnFileExists()
        {
            var actual = _fileWrapper.Exists(_path);

            Assert.That(actual, Is.True);
        }

        [Test]
        public void ShouldReadContents()
        {
            var expected = File.ReadAllText(_path);
            var actual = _fileWrapper.GetFileContents(_path);

            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}
