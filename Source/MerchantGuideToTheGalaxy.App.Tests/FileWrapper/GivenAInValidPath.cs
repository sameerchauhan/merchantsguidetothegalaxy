using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.App.Tests.FileWrapper
{
    [TestFixture()]
    public class GivenAInValidPath
    {
        private MerchantGuideToTheGalaxy.App.FileWrapper _fileWrapper;
        private string _path;

        [SetUp]
        public void SetUp()
        {
            _fileWrapper = new MerchantGuideToTheGalaxy.App.FileWrapper();
            _path = "llklk lklk ";
        }

        [Test]
        public void ShouldReturnNoFileExists()
        {
            var actual = _fileWrapper.Exists(_path);

            Assert.That(actual, Is.False);
        }      
    }
}