using System;
using System.Collections.Generic;
using System.Linq;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantGuideToTheGalaxy.Expressions
{
    public class TranslationExpression : IExpression
    {
        private const int NumberOfWords = 3;
        private readonly Dictionary<string, string> _dictionary;

        public TranslationExpression(Dictionary<string, string> dictionary)
        {
            _dictionary = dictionary;
        }

        public bool MatchesFormat(string expression)
        {
            var expressionComponents = expression.Split(" ".ToCharArray(), 
                StringSplitOptions.RemoveEmptyEntries).ToList();
            return expressionComponents.Count == NumberOfWords;
        }

        public bool Process(string expression)
        {
            try
            {
               var expressionComponents = expression.Split(" ".ToCharArray(),
               StringSplitOptions.RemoveEmptyEntries).ToList();
                var galactic = expressionComponents[0].Trim();
                if (!_dictionary.ContainsKey(galactic))
                {
                    var romanNumeral = expressionComponents[2].Trim();
                    _dictionary.Add(galactic, romanNumeral);
                }
                return true;
            }
            catch (Exception error)
            {
                //log error
                return false;
            }
           
        }
    }
}