﻿namespace MerchantGuideToTheGalaxy.App.Core
{
    public interface IController
    {
        void Initialise();
        string Process(string content);
    }
}