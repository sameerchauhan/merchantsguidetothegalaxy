namespace MerchantGuideToTheGalaxy.App.Core
{
    public interface IApp
    {
        void Initialise();
    }
}
