﻿namespace MerchantGuideToTheGalaxy.App.Core
{
    public interface IFileWrapper
    {
        string GetFileContents(string path);
        bool Exists(string path);
    }
}