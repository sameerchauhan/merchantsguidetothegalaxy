using System;
using System.Collections.Generic;
using MerchantGuideToTheGalaxy.Expressions;
using MerchantsGuideToTheGalaxy.Core;
using MerchantsGuideToTheGalaxy.Symbols;

namespace MerchantGuideToTheGalaxy.App
{
    public class TypeResolver : ITypeResolver 
    {
        public T Resolve<T>(params object[] dependencies) where T : class
        {
            if (typeof (T) == typeof (IExpressionIntepreter))
            {
                var priceList = new Dictionary<string, decimal>();
                var output = dependencies[0] as List<string>;
                var dictionary = new Dictionary<string, string>();
                var translator = new Translator(dictionary);
                var decimalConverter = DecimalConverter.CreateDefault();

                var expressionTree = new List<IExpression>
                {
                    new TranslationExpression(dictionary),
                    new TransactionExpression(priceList, decimalConverter, translator),
                    new QueryExpression(decimalConverter, output, translator),
                    new CreditQueryExpression(decimalConverter, priceList, output, translator),
                    new UnknownExpression(output)
                };

                return new ExpressionIntepreter(expressionTree) as T;
            }

            throw new NotSupportedException();
        }
    }

    public interface ITypeResolver
    {
        T Resolve<T>(params object[] dependencies) where T : class;
    }
}