using System.Collections.Generic;
using System.Linq;
using System.Text;
using MerchantGuideToTheGalaxy.App.Core;
using MerchantsGuideToTheGalaxy.Core;

namespace MerchantGuideToTheGalaxy.App
{
    public class Controller : IController
    {
        private readonly IView _view;
        private readonly IExpressionIntepreter _expressionIntepreter;
        private readonly List<string> _output;

        public Controller(IView view, IExpressionIntepreter expressionIntepreter, List<string> output)
        {

            _view = view;
            _expressionIntepreter = expressionIntepreter;
            _output = output;
        }
        
        public Controller(IExpressionIntepreter expressionIntepreter, List<string> output)
        {
            _expressionIntepreter = expressionIntepreter;
            _output = output;
        }

        public void Initialise()
        {

            _view.Initialise(this);
        }

        public string Process(string content)
        {
            _output.Clear();
            _expressionIntepreter.ProcessExpressions(content);
            var model = new StringBuilder();
            if (_output.Any())
            {
                foreach (var message in _output)
                {
                    model.AppendLine(message);
                }

                return model.ToString();
            }
            return string.Empty;
        }
    }
}