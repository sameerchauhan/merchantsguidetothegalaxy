namespace MerchantsGuideToTheGalaxy.Core
{
    public interface ITranslator
    {
        string Translate(string expression);
    }
}