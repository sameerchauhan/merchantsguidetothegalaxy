using System;
using System.Collections.Generic;
using System.Linq;

namespace MerchantsGuideToTheGalaxy.Core
{
    public abstract class SubtractableSymbol : Symbol
    {
        private readonly List<Symbol> _subtractableFromSymbol;

        protected SubtractableSymbol(string identifier, int value, List<Symbol> subtractableFromSymbol) : 
            base(identifier, value)
        {
            _subtractableFromSymbol = subtractableFromSymbol;
        }

        private void Validate(string romanNumeral)
        {
            romanNumeral = romanNumeral.Replace(" ", string.Empty);
            var firstInstance = romanNumeral.IndexOf(Identifer, StringComparison.Ordinal);
            var lastInstance = romanNumeral.LastIndexOf(Identifer, StringComparison.Ordinal);
            var maximumDistanceBetweenFirstAndLastInstanceOfSymbol = 3;
            var notFound = -1;

            ValidateNotRepeatedFourConsecutiveTimes(lastInstance, firstInstance,
                maximumDistanceBetweenFirstAndLastInstanceOfSymbol);
            

            if (_subtractableFromSymbol != null)
            {
                ValidateSymbolWhenSymbolBeforeSymbolWithMoreValue2(romanNumeral, notFound, firstInstance);
            }
        }

        private static void ValidateNotRepeatedFourConsecutiveTimes(int lastInstance, int firstInstance,
            int maximumDistanceBetweenFirstAndLastInstanceOfSymbol)
        {
            if ((lastInstance - firstInstance) == maximumDistanceBetweenFirstAndLastInstanceOfSymbol)
            {
                throw new FormatException("Cannot have the symbol four consecutive times ");
            }
        }

        private void ValidateSymbolWhenSymbolBeforeSymbolWithMoreValue2(string romanNumeral, int notFound,
            int firstInstance)
        {            
            foreach (var symbol in _subtractableFromSymbol)
            {
                var indexOfSubtractableSymbol = romanNumeral.IndexOf(symbol.Identifer, StringComparison.Ordinal);
                if (indexOfSubtractableSymbol == notFound || indexOfSubtractableSymbol < firstInstance)
                {
                    continue;
                }

                if ((firstInstance + Identifer.Length) != indexOfSubtractableSymbol)
                {
                    throw new FormatException($"Cannot have the {Identifer} repeated twice before {symbol}");
                }
            }
        }

        public override int? Convert(string romanNumeral)
        {
            if (romanNumeral.Contains(Identifer))
            {
                Validate(romanNumeral);
                return CalculateValue(romanNumeral);
            }

            return null;
        }

        private int? CalculateValue(string romanNumeral)
        {
            var symbols = romanNumeral.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var sum = 0;
            var previousSymbol = string.Empty;
            foreach (var symbol in symbols)
            {
                if (symbol == Identifer)
                {
                    sum += Value;
                }
                    
                if (_subtractableFromSymbol != null && 
                    _subtractableFromSymbol.FirstOrDefault(e => e.Identifer == symbol) != null &&
                    previousSymbol == Identifer)
                {
                    sum = (-1*Value);
                }

                previousSymbol = symbol;
            }
            return sum;
        }
    }
}