using System.Collections.Generic;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.TranslationExpression
{
    [TestFixture()]
    public class GivenAnErrorOccurs
    {
        [Test]
        public void ShouldReturnFalse()
        {
            var expression = "globisI";
            var dictionary = new Dictionary<string, string>();
            var translationExpression = new Expressions.TranslationExpression(dictionary);
            var actual = translationExpression.Process(expression);

            Assert.That(actual, Is.False);
        }
    }
}