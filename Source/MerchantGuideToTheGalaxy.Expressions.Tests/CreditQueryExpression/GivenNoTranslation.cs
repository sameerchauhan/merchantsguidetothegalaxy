using System.Collections.Generic;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.CreditQueryExpression
{
    [TestFixture(Category = "CreditQueryExpression")]
    public class GivenNoTranslation
    {
        [Test]
        public void ShouldReturnFalse()
        {
            var output = new List<string>();
            var converter = DecimalConverter.CreateDefault();
            Dictionary<string, decimal> priceList = new Dictionary<string, decimal> { { "Silver", 17 } };
            var translator = new Expressions.Translator(new Dictionary<string, string>());
            var creditQueryExpression = new Expressions.CreditQueryExpression(converter, priceList, output, translator);
            var actual = creditQueryExpression.Process("how many Credits is glob prok Silver ?");

            Assert.That(actual, Is.False);
        }
    }
}