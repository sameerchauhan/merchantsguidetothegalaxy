using System.Collections.Generic;
using MerchantGuideToTheGalaxy.Test.Helpers;
using MerchantsGuideToTheGalaxy.Symbols;
using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.CreditQueryExpression
{
    [TestFixture(Category = "CreditQueryExpression")]
    public class GivenAStringCreditQueryExpression
    {
        [Test]
        public void ShouldMatchOnTheWordsHowManyCreditsIs()
        {
            var output = new List<string>();
            var converter = DecimalConverter.CreateDefault();
            Dictionary<string, decimal> priceList = new Dictionary<string, decimal> { { "Silver", 17 } };
            var translator = new Expressions.Translator(TestHelper.GetDictionary());
            var creditQueryExpression = new Expressions.CreditQueryExpression(converter, priceList, output, translator);
            var actual = creditQueryExpression.MatchesFormat("how many Credits is");

            Assert.That(actual, Is.True);
        }
    }
}