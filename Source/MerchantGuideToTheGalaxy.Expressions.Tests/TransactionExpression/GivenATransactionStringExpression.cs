using NUnit.Framework;

namespace MerchantGuideToTheGalaxy.Expressions.Tests.TransactionExpression
{
    [TestFixture]
    public class GivenATransactionStringExpression
    {
        [Test]
        public void ShouldMatchIfEndsWithCredit()
        {
            var expression = "glob glob Silver is 34 Credits";
            var translationExpression = new Expressions.TransactionExpression(null, null, null);
            var matchesFormat = translationExpression.MatchesFormat(expression);

            Assert.That(matchesFormat, Is.True);
        }
    }
}